// ==UserScript==
// @name         Wikipedia Preview
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://en.wikipedia.org/wiki/*
// @require      https://code.jquery.com/jquery-3.2.1.js
// @noframes
// @grant        none
// ==/UserScript==

/*
This script allows the user to preview wikipedia pages by hovering over any link in a wikipedia article and holding 'z'
The linked article will display as long as the key is held, and can be scrolled through.
*/
(function() {
    'use strict';
    const KEY = 90; //z
    var show = false; //iframe is showing
    var _url = ""; //iframe source url
    $("body").prepend("<iframe id='popup' src='' style='display:none;z-index:1000;position:fixed;' width='100%' height='100%' scrolling='auto'></iframe>");

    //parse title and change frame source on link mouseover
    $("a").each(function(){
        $(this).mouseover(
            function(e) {
                try{
                    _url = "https://en.wikipedia.org/wiki/" +  $(this).attr("title").split(" ").join("_");
                }catch(err) {console.log("element has no title attribute");}
            });
    });


    //show frame when holding key
    $(document).keydown(function(e){
        if (e.which != KEY || show) return;
        $("#popup").attr("src", _url).fadeIn();
        show = true;
    });
    //fade out frame on key up
    $(document).keyup(function(e) {
        if (e.which == KEY) {
            $("#popup").fadeOut();
            show = false;
        }
    });
})();