// ==UserScript==
// @name         Image Rain
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        */*
// @require      https://code.jquery.com/jquery-3.2.1.js
// @noframes
// ==/UserScript==

/*
Press the ` (backtick) key to make an image rain across the screen
The speed and direction of the animation can be controlled using the '1' and '2 keys
*/
(function() {
    'use strict';
    $(document).ready( function() {

        var running = false;
        var global_speed = 20.0;

        function anim(num_logos) {

            //create images with randomized speed and starting position, using first <img> on page as source
            $("body").append("<div id=\"test\"></div>");
            for (var i = 0; i < num_logos; i++) {
                var idStr = "logo" + i;
                var leftStr = "left: " + Math.random() * Math.floor(window.innerWidth) + "px;";
                var topStr = "top: " + Math.random() * Math.floor(window.innerHeight) + "px;";
                var speed = String(5+ (Math.random() * Math.floor(50)));
                var src = $("img").attr("src");
                if (src == null) src = "https://www.lyonscg.com/wp-content/themes/lyons/assets/images/logo.svg";
                var htmlStr = "<img data-left=\"" + leftStr + "\" data-speed=\"" + speed + "\"id=\"" + idStr + "\" style=\"" + topStr + "position:fixed;" + leftStr + "\" src =\"" + src + "\" height=\"40\" width=\"80\">";
                $("#test").append(htmlStr);
            }

            //animate images
            var id = setInterval(frame, 50);
            function frame() {
                for (var i = 0; i < num_logos; i++) {
                    var logo = document.getElementById("logo" + i);
                    if (logo != null) {
                        var sp = Math.ceil((global_speed*0.01) * parseFloat(logo.getAttribute("data-speed")));
                        if (sp == 0) {
                            if (global_speed < 0) sp = -1;
                            if (global_speed > 0) sp = 1;
                        }
                        var topVal = parseInt(logo.style.top, 10);
                        var leftVal = parseInt(logo.style.left, 10);
                        logo.style.top = (topVal+sp)+ "px";
                        logo.style.left = (leftVal+sp)+ "px";

                        //if image goes off screen, wrap back to start
                        if (((topVal > window.innerHeight - 5 || leftVal > window.innerWidth - 50) && global_speed > 0) ||
                           ((topVal < 0 || leftVal < 0) && global_speed < 0)) {
                            logo.style.top = String(window.innerHeight - topVal) + "px";
                            logo.style.left = String(window.innerWidth - leftVal) + "px";
                        }
                    }
                }
            }
        }
        //stop and start animation on keypress
        //controls: '`' (backtick) key to start/stop, '1' to slow down, '2' to speed up
        $(document).keypress(function(e) {
            console.log(e.which);
            //change speed
            if (running) {
                if (e.which == 49) global_speed-= 5;
                if (e.which == 50) global_speed+= 5;
            }
            //start/stop animation
            if (e.which == 96) {
                if (!running) {
                    running = true;
                    anim(100);
                }
                else {
                    running = false;
                    global_speed = 20.0;
                    document.getElementById("test").remove();
                }
            }
        });
    });
})();