// ==UserScript==
// @name         LyonsCG Google Override
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.google.com/*
// @require      https://code.jquery.com/jquery-3.2.1.js
// @grant        none
// @noframes
// ==/UserScript==

/*
This script replaces the Google logo with the LYONSCG logo, and adds a button that links to the LYONSCG website.
*/
(function() {
    'use strict';

    $(document).ready( function() {
        //change logo image
        $("img:not(._bGb)").attr("src", "https://www.lyonscg.com/wp-content/themes/lyons/assets/images/logo.svg");
        $("img:not(._bGb)").attr("srcset", "https://www.lyonscg.com/wp-content/themes/lyons/assets/images/logo.svg");

        //add Lyons home button
        $("div.jsb").find("center").append(
            `<form style=\"display: inline;\" method=\"POST\" action=\"http://www.lyonscg.com\">
                <input value=\"LyonsCG\" type=\"submit\" ></input>
            </form>`);
    });
})();