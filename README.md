# README #

This repository contains several "useful" (not really) TamperMonkey userscripts


Scripts:

	Image_Rain
	
		Creates an animation of images raining across the browser window using an
		image source from the current webpage. The speed and direction of animation
		can be controlled using the keyboard. Works on all webpages.
		
	Wikipedia_Preview
	
		Allows the user to hover over any link on a wikipedia page and preview the
		corresponding article without navigating away from the current page.
		Works on wikipedia.org only
		
	LyonsCG_Google_Override
	
		Replaces the Google logo with the LYONSCG logo and adds a button that links
		to the LYONSCG website. Works on Google.com only.